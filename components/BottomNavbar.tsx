import React from "react";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import StoreIcon from "@material-ui/icons/Store";
import ReceiptIcon from "@material-ui/icons/Receipt";
import LiveHelpIcon from "@material-ui/icons/LiveHelp";
import PersonIcon from "@material-ui/icons/Person";
import styles from "./style.module.css";

export default function SimpleBottomNavigation() {
  const [value, setValue] = React.useState(0);

  return (
    <BottomNavigation
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);
      }}
      style={{ maxWidth: 444, width: "100%", border: "1px solid #e0e0e0" }}
      showLabels
    >
      <BottomNavigationAction
        label="Belanja"
        icon={<StoreIcon />}
        classes={{ selected: styles.selected }}
      />
      <BottomNavigationAction
        label="Transaksi"
        icon={<ReceiptIcon />}
        classes={{ selected: styles.selected }}
      />
      <BottomNavigationAction
        label="Bantuan"
        icon={<LiveHelpIcon />}
        classes={{ selected: styles.selected }}
      />
      <BottomNavigationAction
        label="Profile"
        icon={<PersonIcon />}
        classes={{ selected: styles.selected }}
      />
    </BottomNavigation>
  );
}
