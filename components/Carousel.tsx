import React from "react";
import Grid from "@material-ui/core/Grid";
import Carousel, { DotProps } from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { makeStyles } from "@material-ui/core";

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 1,
    slidesToSlide: 1,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 1,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};

const style = makeStyles({
  dotList: {
    display: "flex",
    justifyContent: "flex-start",
    padding: "4px 8px",
  },
});

type Banner = {
  guid: {
    rendered: string;
  };
};

type CarProps = {
  data: Banner[];
};

function Slider(props: CarProps) {
  const classes = style();
  const { data } = props;
  return (
    <Grid style={{ padding: 10 }}>
      {data?.length > 0 && (
        <Carousel
          containerClass="container-heads"
          itemClass="item-container"
          showDots
          customDot={<CustomDot />}
          responsive={responsive}
          autoPlay
          infinite
          swipeable
          draggable
          arrows={false}
          dotListClass={classes.dotList}
        >
          {data?.map((item) => (
            <img
              src={item.guid.rendered}
              style={{
                display: "flex",
                backgroundSize: "cover",
                width: "inherit",
                borderRadius: 5,
              }}
            />
          ))}
        </Carousel>
      )}
    </Grid>
  );
}

const CustomDot = ({ index, active, onClick, carouselState }: DotProps) => {
  return (
    <div onClick={() => onClick}>
      <button
        onClick={() => onClick()}
        style={{
          height: 12,
          width: active ? 24 : 12,
          backgroundColor: active ? "#ffffffff" : "rgb(228, 230, 231)",
          borderRadius: active ? 15 : "50%",
          border: "none",
          marginRight: 8,
        }}
      ></button>
    </div>
  );
};

// const CustomDot = ({ onClick, ...rest }) => {
//   const {
//     onMove,
//     index,
//     active,
//     carouselState: { currentSlide, deviceType },
//   } = rest;
//   // onMove means if dragging or swiping in progress.
//   // active is provided by this lib for checking if the item is active or not.
//   return (
//     <button
//       onClick={() => onClick()}
//       style={{
//         height: 12,
//         width: active ? 24 : 12,
//         backgroundColor: active ? "#ffffffff" : "rgb(228, 230, 231)",
//         borderRadius: active ? 15 : "50%",
//         border: "none",
//         marginRight: 8,
//       }}
//     ></button>
//   );
// };

export default Slider;
