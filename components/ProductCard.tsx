import React, { useState, useEffect, useContext } from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { Typography } from "@material-ui/core";
import { CartFoodContext } from "../context/CartContext2";

// type Products = {
//   images: [
//     {
//       src: any;
//     }
//   ];
//   name: string;
//   price: string;
//   meta_data: {
//     value: string;
//   };
// };

function ProductLists({ products }) {
  const { cart, addCart, increaseCart, decreaseCart, cartUpdated } = useContext(
    CartFoodContext
  );
  console.log(increaseCart);
  console.log(decreaseCart);
  // const [list, setList] = useState([]);

  // const getData = async () => {
  //   const base_url = await axios(
  //     "https://api.tumbasin.id/t/v1/products?vendor=1003&featured=true"
  //   );
  //   setList(base_url.data);
  // };

  // useEffect(() => {
  //   getData();
  // }, []);

  // console.log(data);

  const [qty, setQty] = useState(0);

  useEffect(() => {
    if (cart && Array.isArray(cart)) {
      const selectedItem = cart.find((item) => item.id == products.id);

      if (selectedItem) {
        setQty(selectedItem.total);
      } else {
        setQty(0);
      }
    }
  }, [cartUpdated, cart, products]);

  return (
    <div>
      <Grid>
        <Grid
          style={{
            display: "flex",
            flexDirection: "row",
            padding: "0 15px",
            marginTop: 20,
          }}
        >
          <Grid item xs={3}>
            <img
              src={products?.images[0].src}
              style={{
                display: "flex",
                width: "-webkit-fill-available",
              }}
            />
          </Grid>
          <Grid
            item
            xs={5}
            style={{
              display: "flex",
              flexDirection: "column",
              marginLeft: "14px",
            }}
          >
            <Grid
              style={{
                whiteSpace: "nowrap",
                fontWeight: 400,
                fontSize: "12px",
              }}
            >
              {products?.name}
            </Grid>
            <Grid style={{ display: "flex", marginTop: 35 }}>
              <Grid
                style={{
                  display: "flex",
                  fontSize: "12px",
                  fontWeight: 700,
                  color: "#F1885B",
                }}
              >
                {products?.price}
              </Grid>
              <Grid
                style={{
                  display: "flex",
                  fontWeight: 800,
                  fontSize: "12px",
                  color: "#C7C7C9",
                  marginLeft: "5px",
                  alignItems: "center",
                }}
              >
                /{products?.meta_data[0].value}
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={4} style={{ alignItems: "flex-end", display: "grid" }}>
            <Grid style={{ justifyContent: "flex-end", display: "flex" }}>
              {qty > 0 ? (
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    marginBottom: 25,
                  }}
                >
                  <Typography
                    style={{
                      width: 25,
                      border: "1px solid #e0e0e0",
                      color: "black",
                      backgroundColor: "white",
                      borderRadius: 4,
                      textAlign: "center",
                      fontSize: 13,
                      padding: "1px 0px",
                    }}
                    onClick={() => {
                      decreaseCart(products);
                    }}
                  >
                    -
                  </Typography>
                  <Typography
                    style={{
                      width: 23,
                      justifyContent: "center",
                      fontSize: 12,
                      display: "flex",
                    }}
                  >
                    {qty}
                  </Typography>
                  <Typography
                    style={{
                      width: 25,
                      border: "1px solid #e0e0e0",
                      color: "white",
                      backgroundColor: "rgb(241, 91, 93)",
                      borderRadius: 4,
                      textAlign: "center",
                      fontSize: 13,
                      padding: "1px 0px",
                    }}
                    onClick={() => {
                      increaseCart(products);
                    }}
                  >
                    +
                  </Typography>
                </div>
              ) : (
                <Button
                  onClick={() => {
                    addCart(products);
                  }}
                  style={{
                    backgroundColor: "#F15B5D",
                    color: "#ffffff",
                    fontSize: 10,
                    marginBottom: 15,
                  }}
                >
                  TAMBAHKAN
                </Button>
              )}
            </Grid>
          </Grid>
        </Grid>
        <div style={{ border: "1px solid #e0e0e0" }} />
      </Grid>
    </div>
  );
}

export default ProductLists;
