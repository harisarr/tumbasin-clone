import React, { useState, useEffect } from "react";
import Grid from "@material-ui/core/Grid";

type Category = {
  id: string;
  name: string;
  slug: string;
  image: {
    src: string;
  };
  price: string;
};

type CategoriesProps = {
  data: Category[];
};

function ProductCategories(props: CategoriesProps) {
  const { data } = props;
  return (
    <div>
      <Grid
        container
        spacing={0}
        style={{
          display: "flex",
          flexDirection: "column",
          padding: "0px 18px",
        }}
      >
        <Grid item xs={12} style={{ color: "#14181B", fontWeight: 700 }}>
          Telusuri Jenis Produk
        </Grid>
        <div
          style={{
            display: "grid",
            alignItems: "center",
            gridTemplateColumns: "auto auto auto auto auto",
            marginTop: "10px",
          }}
        >
          {data?.slice(0, 6).map((category: Category) => {
            return (
              <div
                key={category.id}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  width: 65,
                  alignItems: "center",
                  boxShadow: "0px 4px 4px 0px #000000 25%",
                }}
              >
                <img
                  src={category.image.src}
                  style={{
                    display: "flex",
                    padding: 3,
                    width: 45,
                    backgroundColor: "#FDF4FC",
                    borderRadius: "5px",
                    boxShadow:
                      "0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)",
                  }}
                />

                <p
                  style={{
                    textAlign: "center",
                    whiteSpace: "nowrap",
                    fontSize: "10px",
                    color: "rgb(112, 117, 133)",
                    marginTop: "4px",
                  }}
                >
                  {category.name}
                </p>
              </div>
            );
          })}

          {data?.slice(6, 9).map((category: Category) => {
            return (
              <div
                key={category.id}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  width: "65px",
                  boxShadow: "0px 4px 4px 0px #000000 25%",
                }}
              >
                <img
                  src={category.image.src}
                  style={{
                    display: "flex",
                    alignSelf: "center",
                    width: "45px",
                    padding: "3px",
                    backgroundColor: "#FDF4FC",
                    borderRadius: "5px",
                    boxShadow:
                      "0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)",
                  }}
                />

                <p
                  style={{
                    textAlign: "center",
                    fontSize: "10px",
                    color: "rgb(112, 117, 133)",
                    width: "auto",
                    marginTop: "4px",
                    marginBottom: 0,
                  }}
                >
                  {category.name}
                </p>
              </div>
            );
          })}
        </div>
      </Grid>
    </div>
  );
}

export default ProductCategories;
