import React from "react";
import Grid from "@material-ui/core/Grid";
import ProductCard from "./ProductCard";

type Products = {
  id: string;
  images: [
    {
      src: any;
    }
  ];
  name: string;
  price: string;
  meta_data: {
    value: string;
  };
};

type ListProps = {
  data: Products[];
};

function ProductLists(props: ListProps) {
  const { data } = props;
  return (
    <div>
      <Grid
        container
        spacing={0}
        style={{ display: "flex", padding: "0px", marginBottom: 100 }}
      >
        <Grid
          item
          xs={12}
          style={{
            display: "flex",
            alignItems: "center",
            marginBottom: 4,
            marginTop: 4,
            padding: "0 15px",
          }}
        >
          <Grid item xs={6} style={{ fontWeight: 700, fontSize: "14px" }}>
            Produk Bestseller
          </Grid>
          <Grid
            item
            xs={6}
            style={{
              fontWeight: 700,
              fontSize: "14px",
              color: "#F15B5D",
              justifyContent: "flex-end",
              display: "flex",
              cursor: "pointer",
            }}
          >
            Lihat Semua
          </Grid>
        </Grid>

        <Grid item xs={12} style={{ display: "flex", flexDirection: "column" }}>
          {data?.slice(0, 5).map((products: Products) => {
            return <ProductCard key={products.id} products={products} />;
          })}
        </Grid>
      </Grid>
    </div>
  );
}

export default ProductLists;
