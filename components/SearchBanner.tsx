import React, { useEffect, useState } from "react";
import InputBase from "@material-ui/core/InputBase";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import StoreIcon from "@material-ui/icons/Store";
import { Typography } from "@material-ui/core";

function SearchBanner() {
  const [scrolled, setScrolled] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 0) {
        setScrolled(true);
        // console.log("scrolled");
      } else {
        setScrolled(false);
      }
    });
  }, []);

  return (
    <div>
      <Grid container spacing={0} style={{ display: "flex" }}>
        <Grid
          item
          xs={12}
          style={{
            display: "flex",
            backgroundColor: "#ffffffff",
            borderRadius: "16px",
            marginTop: "20px",
            marginRight: "24px",
            marginLeft: "24px",
          }}
        >
          <Grid item xs={2} style={{ display: "flex" }}>
            <Button style={{ borderRadius: "20px", margin: "-5px" }}>
              <img src="/icon/search-icon.png" />
            </Button>
          </Grid>
          <Grid item xs={10}>
            <InputBase
              placeholder="Search..."
              style={{
                width: "100%",
                fontSize: "16px",
              }}
            />
          </Grid>
        </Grid>

        <Grid
          item
          xs={12}
          style={{
            borderRadius: "5px",
            backgroundColor: "#ffffff",
            margin: "18px",
            padding: "10px 20px",
            boxShadow:
              "0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)",
          }}
        >
          <p
            style={{
              fontSize: "0.875rem",
              marginTop: "0px",
              marginBottom: "0.35em",
              color: "rgb(78, 83, 86)",
            }}
          >
            Kamu Belanja Di :
          </p>
          <Grid
            style={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <Grid item xs={8} style={{ display: "flex", alignItems: "center" }}>
              <StoreIcon
                style={{ color: "rgb(135, 202, 254)", fontSize: "30px" }}
              />
              <Typography
                style={{
                  fontWeight: 700,
                  fontSize: "0.875rem",
                  margin: "0px 14px",
                }}
              >
                Pasar Gunungpati
              </Typography>
            </Grid>
            <Grid
              item
              xs={4}
              style={{ justifyContent: "flex-end", display: "flex" }}
            >
              <Button
                style={{
                  fontSize: "0.8125rem",
                  backgroundColor: "#F15B5D",
                  color: "white",
                  marginRight: "20px",
                  padding: "3px",
                  boxShadow:
                    "0px 3px 1px -2px rgb(0 0 0 / 20%), 0px 2px 2px 0px rgb(0 0 0 / 14%), 0px 1px 5px 0px rgb(0 0 0 / 12%)",
                }}
              >
                GANTI
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

export default SearchBanner;
