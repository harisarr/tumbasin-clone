import React from "react";
import ContentLoader from "react-content-loader";

const Skeleton = (props) => (
  <ContentLoader
    speed={2}
    viewBox="-3 0 425 200"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    style={{ width: "100%", maxWidth: 444 }}
    {...props}
  >
    <rect x="5" y="58" rx="4" ry="4" width="50" height="50" />
    <rect x="5" y="118" rx="4" ry="4" width="50" height="10" />
    <rect x="87" y="58" rx="4" ry="4" width="50" height="50" />
    <rect x="87" y="118" rx="4" ry="4" width="50" height="10" />
    <rect x="169" y="58" rx="4" ry="4" width="50" height="50" />
    <rect x="169" y="118" rx="4" ry="4" width="50" height="10" />
    <rect x="256" y="57" rx="4" ry="4" width="50" height="50" />
    <rect x="256" y="117" rx="4" ry="4" width="50" height="10" />
    <rect x="340" y="57" rx="4" ry="4" width="50" height="50" />
    <rect x="340" y="117" rx="4" ry="4" width="50" height="10" />
    <rect x="88" y="140" rx="4" ry="4" width="50" height="50" />
    <rect x="88" y="200" rx="4" ry="4" width="50" height="10" />
    <rect x="171" y="140" rx="4" ry="4" width="50" height="50" />
    <rect x="171" y="200" rx="4" ry="4" width="50" height="10" />
    <rect x="257" y="140" rx="4" ry="4" width="50" height="50" />
    <rect x="257" y="200" rx="4" ry="4" width="50" height="10" />
    <rect x="4" y="141" rx="4" ry="4" width="50" height="50" />
    <rect x="4" y="200" rx="4" ry="4" width="50" height="10" />
    <rect x="5" y="19" rx="0" ry="0" width="171" height="13" />
  </ContentLoader>
);

export default Skeleton;
