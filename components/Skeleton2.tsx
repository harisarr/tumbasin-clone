import React from "react";
import ContentLoader from "react-content-loader";

const SkeletonProduct = (props) => (
  <ContentLoader
    speed={2}
    width={425}
    viewBox="-3 0 425 300"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    style={{ width: "100%", maxWidth: 444 }}
    {...props}
  >
    <rect x="6" y="10" rx="0" ry="0" width="128" height="10" />
    <rect x="313" y="10" rx="0" ry="0" width="100" height="9" />
    <rect x="5" y="40" rx="4" ry="4" width="80" height="70" />
    <rect x="100" y="40" rx="4" ry="4" width="315" height="70" />
    <rect x="5" y="120" rx="4" ry="4" width="80" height="70" />
    <rect x="100" y="120" rx="4" ry="4" width="315" height="70" />
  </ContentLoader>
);

export default SkeletonProduct;
