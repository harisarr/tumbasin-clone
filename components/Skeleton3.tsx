import React from "react";
import ContentLoader from "react-content-loader";

const SkeletonBanner = (props) => (
  <ContentLoader
    speed={2}
    width={425}
    viewBox="-3 0 425 200"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    style={{
      width: "100%",
      maxWidth: 444,
    }}
    {...props}
  >
    <rect x="2" y="10" rx="5" ry="5" width="410" height="160" />
  </ContentLoader>
);

export default SkeletonBanner;
