import { createContext, useState } from "react";

export interface CartFood {
  id: string;
  name: string;
  images: {
    src: string;
  };
  meta_data: string;
  price: number;
  quantity: number;
  total?: number;
  totalPrice?: number;
}

export interface CartFoodType {
  price: number;
  increaseCart: any;
  decreaseCart: any;
  cart: CartFood[];
  restoreCart: () => void;
  cartUpdated: boolean;
  calculateTotal: (list: any) => void;
  addCart: (list: any) => void;
}

export const CartFoodContext = createContext<CartFoodType>({} as never);

const CartFoodContextProvider = ({ children }) => {
  const [cart, setCart] = useState([]);
  const [price, setPrice] = useState(0);
  const [cartUpdated, setCartUpdated] = useState(true);

  const restoreCart = () => {
    const cartData = localStorage.getItem("cart");
    const priceData = JSON.parse(localStorage.getItem("price"));
    if (cartData) {
      setCart(JSON.parse(cartData));
      setPrice(priceData);
    }
  };

  const calculateTotal = (list) => {
    if (list?.length > 0) {
      const sum = (item) => item.reduce((x, y) => x + y);
      let total = sum(list.map((product) => Number(product.totalPrice)));
      setPrice(total);
      localStorage.setItem("price", JSON.stringify(total));
      setCartUpdated(!cartUpdated);
    } else {
      setPrice(0);
      localStorage.setItem("price", JSON.stringify(0));
      setCartUpdated(!cartUpdated);
    }
  };

  const addCart = (item: any) => {
    const cartData = [...cart, { ...item, total: 1, totalPrice: item.price }];
    setCart(cartData);
    calculateTotal(cartData);
    localStorage.setItem("cart", JSON.stringify(cartData));
  };

  const increaseCart = (item: any) => {
    const objIndex = cart.findIndex((select) => {
      return select.id === item.id;
    });
    let cartData = cart;
    cartData[objIndex].total += 1;
    cartData[objIndex].totalPrice =
      cartData[objIndex].total * cartData[objIndex].price;
    setCart(cartData);
    calculateTotal(cartData);
    localStorage.setItem("cart", JSON.stringify(cartData));
    localStorage.getItem("cart");
  };

  const decreaseCart = (item: any) => {
    const objIndex = cart.findIndex((select) => {
      return select.id === item.id;
    });
    let cartData = cart;
    if (cartData[objIndex].total > 1) {
      cartData[objIndex].total -= 1;
      cartData[objIndex].totalPrice =
        cartData[objIndex].total * cartData[objIndex].price;
      setCart(cartData);
      calculateTotal(cartData);
      localStorage.setItem("cart", JSON.stringify(cartData));
    } else {
      const newCart = cart.filter((obj) => {
        return obj.id !== item.id;
      });
      setCart(newCart);
      calculateTotal(newCart);
      localStorage.setItem("cart", JSON.stringify(newCart));
    }
  };

  return (
    <CartFoodContext.Provider
      value={{
        cart,
        price,
        restoreCart,
        calculateTotal,
        addCart,
        increaseCart,
        decreaseCart,
        cartUpdated,
      }}
    >
      {children}
    </CartFoodContext.Provider>
  );
};

export default CartFoodContextProvider;
