import React from "react";
import Skeleton from "../components/Skeleton";
import Skeleton3 from "../components/Skeleton3";
import Skeleton2 from "../components/Skeleton2";

const SkeletonEdit = () => {
  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <Skeleton />
      <Skeleton3 />
      <Skeleton2 />
    </div>
  );
};

export default SkeletonEdit;
