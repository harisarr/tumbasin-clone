import React, { useEffect, useContext } from "react";
import Head from "next/head";
import { AppProps } from "next/app";
import { ThemeProvider } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import theme from "../theme";
import { QueryClientProvider, QueryClient } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import CartContextProvider, { CartFoodContext } from "../context/CartContext2";

function Index({ children }) {
  const { restoreCart } = useContext(CartFoodContext);

  useEffect(() => {
    restoreCart();
  }, []);

  return <div>{children}</div>;
}

export default function MyApp(props: AppProps) {
  const { Component, pageProps } = props;
  const queryClient = new QueryClient();

  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement!.removeChild(jssStyles);
    }
  }, []);

  return (
    <React.Fragment>
      <Head>
        <title>Tumbasin Clone</title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />
      </Head>
      <ThemeProvider theme={theme}>
        <CartContextProvider>
          <QueryClientProvider client={queryClient}>
            <CssBaseline />
            <Index>
              <Component {...pageProps} />
            </Index>
            <ReactQueryDevtools />
          </QueryClientProvider>
        </CartContextProvider>
      </ThemeProvider>
    </React.Fragment>
  );
}
