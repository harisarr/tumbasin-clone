import ProductCategories from "../components/ProductCategories";
import ProductLists from "../components/ProductLists";
import SearchBanner from "../components/SearchBanner";
import Slider from "../components/Carousel";
import { useEffect, useState, useContext } from "react";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Grow from "@material-ui/core/Grow";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import SearchIcon from "@material-ui/icons/Search";
import BottomNavigationMob from "../components/BottomNavbar";
import StorefrontIcon from "@material-ui/icons/Storefront";
import { CartFoodContext } from "../context/CartContext2";
import Typography from "@material-ui/core/Typography";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import { getProducts } from "../services/ProductListsService";
import { getCategory } from "../services/ProductCategoriesService";
import { getBanner } from "../services/CarouselService";
import { useQuery } from "react-query";
import Skeleton2 from "../components/Skeleton2";
import Skeleton from "../components/Skeleton";
import Skeleton3 from "../components/Skeleton3";

export default function Home() {
  const [scrolled, setScrolled] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 170) {
        setScrolled(true);
        // console.log("scrolled");
      } else {
        setScrolled(false);
      }
    });
  }, []);

  const {
    data: products,
    isError,
    isLoading: loadingProduct,
    isFetching,
    isSuccess,
  } = useQuery("products", getProducts);

  const { data: category, isLoading: loadingCategory } = useQuery(
    "category",
    getCategory
  );
  const { data: banners, isLoading: loadingBanner } = useQuery(
    "banner",
    getBanner
  );

  console.log(banners);

  const { cart, price } = useContext(CartFoodContext);
  const isLoading = loadingProduct && loadingBanner && loadingCategory;
  return (
    <Container
      maxWidth="xs"
      style={{ padding: 0, border: "1px solid #e0e0e0", height: "100%" }}
    >
      <div style={{ width: "auto" }}>
        <Grow in={!scrolled}>
          <div>
            <img
              src="/frame/search-banner.svg"
              style={{
                width: "100%",
                height: 120,
                objectFit: "cover",
              }}
            />
            <div style={{ marginTop: "-121px" }}>
              <SearchBanner />
            </div>
          </div>
        </Grow>
        {scrolled === true && (
          <Grid
            style={{
              display: "flex",
              alignItems: "center",
              position: "fixed",
              top: 0,
              zIndex: 999,
              background: "#ffffff",
              borderBottom: "1px solid #e0e0e0",
              boxShadow:
                "0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)",
              maxWidth: 442,
              width: "100%",
              height: 48,
              animation: "ease-in-out 5s",
              padding: "0 24px",
              borderRadius: 5,
            }}
          >
            <Grid item xs={12} style={{ display: "flex" }}>
              <Grid
                item
                xs={1}
                style={{
                  display: "flex",
                  alignItems: "center",
                }}
              >
                <StorefrontIcon
                  style={{
                    width: 30,
                    color: "#87cafe",
                  }}
                />
              </Grid>
              <Grid item xs={10} style={{ display: "flex" }}>
                <p
                  style={{
                    display: "flex",
                    alignItems: "center",
                    fontWeight: 700,
                    fontSize: "0.875rem",
                    margin: "0px 5px",
                    paddingLeft: 14,
                  }}
                >
                  Pasar Gunungpati
                </p>
                <KeyboardArrowDownIcon
                  style={{
                    fontSize: 30,
                    marginTop: "0px",
                    marginBottom: "0px",
                    color: "#707585",
                    marginLeft: 65,
                  }}
                />
              </Grid>
              <Grid
                item
                xs={1}
                style={{
                  display: "flex",
                  alignItems: "center",
                }}
              >
                <SearchIcon
                  style={{
                    fontSize: 24,
                    marginTop: "0px",
                    marginBottom: "0px",
                    color: "#707585",
                    justifyContent: "flex-end",
                  }}
                />
              </Grid>
            </Grid>
          </Grid>
        )}

        {isLoading ? (
          <>
            <Skeleton />
            <Skeleton3 />
            <Skeleton2 />
          </>
        ) : (
          <>
            <ProductCategories data={category} />
            <Slider data={banners} />
            <ProductLists data={products} />
          </>
        )}

        {cart?.length > 0 ? (
          <Grid
            style={{ backgroundColor: "white", display: "flex", width: "100%" }}
          >
            <Grid
              style={{
                backgroundColor: "#ffffff",
                display: "flex",
                position: "fixed",
                bottom: 56,
                maxWidth: 442,
                width: "100%",
              }}
            >
              <Grid
                style={{
                  display: "flex",
                  background: "rgb(241, 91, 93)",
                  margin: "0px 5px",
                  padding: "5px 20px",
                  paddingRight: 30,
                  width: "100%",
                  borderRadius: 5,
                  maxHeight: 37,
                  height: "100%",
                  marginBottom: 14,
                }}
              >
                <Grid
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    width: "100%",
                  }}
                >
                  <Grid>
                    <Grid style={{ display: "flex" }}>
                      <Typography
                        style={{ fontSize: 10, margin: 0, color: "white" }}
                      >
                        {cart?.length}
                      </Typography>
                      <Typography
                        style={{
                          fontSize: 10,
                          margin: 0,
                          color: "white",
                          padding: "0px 1px",
                        }}
                      >
                        Item |
                      </Typography>
                      <Typography
                        style={{ fontSize: 10, margin: 0, color: "white" }}
                      >
                        Rp {price}
                      </Typography>
                    </Grid>
                  </Grid>
                  <Grid>
                    <Typography
                      style={{ fontSize: 8, margin: 0, color: "white" }}
                    >
                      Pasar Bulu Semarang
                    </Typography>
                  </Grid>
                </Grid>
                <Grid
                  style={{
                    display: "flex",
                    alignItems: "flex-end",
                    color: "white",
                  }}
                >
                  <ShoppingCartIcon />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        ) : null}

        <div
          style={{
            display: "flex",
            position: "fixed",
            bottom: 0,
            width: "100%",
            maxWidth: 444,
          }}
        >
          <BottomNavigationMob />
        </div>
      </div>
    </Container>
  );
}
