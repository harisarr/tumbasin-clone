export const getBanner = async () => {
  const URL =
    "https://dashboard.tumbasin.id/wp-json/wp/v2/media?media_category[]=39";
  const response = await fetch(URL);
  if (!response.ok) {
    throw new Error("Fetching Error");
  }
  return await response.json();
};
