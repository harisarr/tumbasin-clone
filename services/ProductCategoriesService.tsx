export const getCategory = async () => {
  const URL = "https://api.tumbasin.id/t/v1/products/categories";
  const response = await fetch(URL);
  if (!response.ok) {
    throw new Error("Fetching Error");
  }
  return await response.json();
};
