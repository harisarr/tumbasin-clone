export const getProducts = async () => {
  const URL = "https://api.tumbasin.id/t/v1/products?vendor=1003&featured=true";
  const response = await fetch(URL);
  if (!response.ok) {
    throw new Error("Fetching Error");
  }
  return await response.json();
};
